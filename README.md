# An Uber Clone

Create a set of REST-based APIs that can simulate the working of the Uber rider application. Specifically, those APIs must be able to:

- Request a new ride.
- Cancel a requested ride.

No other functionality is to be built. Examples of typical request/responses are given below. Note that these are only illustrations. **You may decide to have completely different request/response formats.**

------

### Request a new ride

`Request`

```json
{
    username: 'vijay',
    from: 'Connaught Place',
    to: 'Gurugram',
    type: 'uberGO'
}
```

`Response`

```json
{
    rideid: 'ghy876546153'
    success: 'true',
    car: 'Maruti Suzuki WagonR',
    registration: 'DL9CH6754',
    driver: 'Krishna Kumar',
    eta: '5 min',
    fare: 'Rs. 457'
}
```

------

### Cancel a requested ride

`Request`

```json
{
    username: 'vijay',
    rideid: 'ghy876546153',
    reason: 'driver delayed'
}
```

`Response`

```json
{
    sucess: 'true',
    cancellationcharge: 'Rs. 80'
}
```

------

Note that this problem statement is _intentionally_ vague. For example, the problem does not tell you about:

1. The URL patterns for these REST APIs
2. Error handling mechanisms
3. Database / data storage mechanism
4. Address validation
5. and so much more...

You need to make these decisions yourself. In the interest of portability though, we suggest that you use either flat files or SQLite database to keep your data. Use Java to build these APIs. 

### Brownie Points

1. If you use Spring Framework in some fashion
2. If you write unit tests
3. If your code has comments
4. If your code is self-documenting
5. If your URL patterns conform to REST specification
6. If your APIs gracefully degrade
7. If you open a pull request to submit your work!